# About

![image of typesetting equipment](https://images.unsplash.com/photo-1515325915697-9279b4f7effc?q=80&w=1932&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)
  *[Image source](https://unsplash.com/photos/gray-assorted-letter-jewelries-in-brown-wooden-organizer-boxes-Oxl_KBNqxGA?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)*
  

## Project Overview


This page is a record of the process of prototyping a FAIR Tech Registry that will assist NRCS staff and partners to quantify benefits of conservation practices. While our primary audience for this prototype is NRCS decision-makers, we also want to make NRCS tools and data more accessible and understandable for the public.  


This initial prototype will focus on:  

* Designing a registry structure that can persist as a long-term, sharable resource
* Cataloging the existing tooling in our ecosystem that has utility for greenhouse gas modeling or quantification of conservation practices
* Presenting information on a comprehensive subset of NRCS tools and resources in a way that supports user and community needs 

We will coordinate with the relevant project teams to define modular components of these tools, document them according to community-defined standards based on FAIR principles, and assess the outcome as a means of negotiating shareability and interoperability across USDA and NRCS. 

## Partnerships and Collaboration

This scope of work was developed out of long-term collaborations between the following project partnerships: 

- [OpenTEAM](https://openteam.community/) - FAIR Tech Ecosystem Registry Collabathon
- [Meridian Institute](https://merid.org/) - Conservation Benefits Catalog 
- [farmOS](https://farmos.org/), [Our-Sci](https://www.our-sci.net/), [Purdue Ag Informatics Lab](http://aginformaticslab.org/) - Producer Operated Data Systems (PODS) & Conservation Cost Database Cooperative Agreements
- [NRCS Soil Division](https://www.nrcs.usda.gov/conservation-basics/natural-resource-concerns/soil) - IRA-GHG Database group



## Background

Beginning in September 2023, OpenTEAM hosted a Collabathon to explore the design of a FAIR Tech Ecosystem Registry. The concept for this registry includes a curated list of relevant tools and components that comply with FAIR principles, as well as creation of business, social, legal, technical, and operational frameworks that make them understandable, useful, and FAIR in practice.

> With this registry, we are working to create a structure for developers in the open ag tech space to share what they’re doing, illustrate its relevance, and provide avenues for others to build off of individual components in transparent ways. During the three months that this Collabathon convened, we examined the relevant roles and personas present in our community, and developed parameters for how a registry or registries would have to function to meet their needs. As a next step, we now intend to take the criteria and tentative structures we developed in this collabathon and apply them to an initial prototype registry targeting a specific, applicable use case.


[***For full documentation of the FAIR Tech Registry Collabathon, see this page.***](https://openteamag.gitlab.io/codesign/fair-registry/)


This Version 1 registry prototype will be focused on cataloging the existing tooling in our ecosystem connected to common farm conventions & API Switchboard concept work, and that has utility for greenhouse gas modeling or quantification of conservation practices. 

Additionally, in order for us to enact the FAIR principles in our community of practice, we are focused on building scaffolding and forms of communication to make these concepts broadly comprehensible for multiple actors across the open, agriculture, and tech realms. This includes attending to process documentation, designing methods for sharing information across contexts, developing and deploying schemas & ontologies for agricultural data and data products, and more. 


