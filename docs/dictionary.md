# Dictionary of Data Fields - Registry

Both the FAIR Tech Registry Collabathon and the Conservation Benefits Catalog produced tentative categories for assessing tools or models, based on community fact-finding. The [Parameter Comparisons table here](https://docs.google.com/spreadsheets/d/12n2J38W3xFIezXSS0wtx5obUhYah8MVi_Lh2vH8EclY/edit#gid=1580521510) relates the categories or criteria identified by both the FAIR Tech Registry working group and the Meridian Conservation Benefits Catalog project as important for attestation.

The following tables show each data field, a description or definition, and the source of the initial field-- either the FAIR Tech Registry Collabathon (FTR), the Conservation Benefits Catalog (CBC), or both. 

## Minimal Categories / Criteria 

| Category             | Description                          | Source     | 
| -----------          | -----------                          | -----------| 
| Name                 | Tool or module name                  |      Both      |
| Short name           | Abbreviation, Including creating one if it doesn’t have one |  CBC          | 
| Agency or Organization    | Who most recently owns and/ or maintains the tool or asset  |         Both   | 
| Asset Type           | Categories include: Dataset, Software model, Standard, Reference, Program, Protocol, Procedure.  Based on what NRCS would call it.                                 |     CBC       |
| Model or software class      | Categories include: Conservation planning, decision support, data access, data collection, data storage, data visualization, expert synthesis, financial/logistics, geospatial analysis, literature aggregation, meta-modeling output, mobile app, model wrapper, primary, species selection.                         |    CBC        |
| Link to asset, source site, documentation  | Widely variable -- Sometimes this is a published paper, sometimes  a user manual.                                |     Both       |
| Text description      | Based on information provided by the team or publisher.   |      Both      | 
| Point of contact, contact email  | While this information is typically listed, it can be unclear if that person is still active or in touch. This could be a useful space for additional info, i.e. 'is this active?'                     |   Both         |
| Date of last update, version number  | Anecdotal note - if it lists a date or version, likely to be more recent/active                |    Both        |
| Metadata             | Very few tools here had machine-readable metadata, but good to link it when it was available.                                 |   Both         |
| Available to the public?    | Ontology list includes: Yes, No, Not yet (beta), Unknown, No longer, Upon request, Partially       |      CBC      |
| Has API?   | Yes/No (sometimes yes but secret) |      Both - in FTR as 'nice to have' info     |
| Built on/built with      | What is the tech stack? What programming language is the tool/module written in? What specific standards or existing ontologies are used?       |   FTR         |
| System requirements      | OS, versions, prerequisite software/environment, memory, storage |  FTR          |
| License   | License & terms of use        | FTR           |
| Statement on data storage, data privacy management      | Requirement for CARE - Mechanism for ‘Disclosure, consent, and control required with respect to secondary uses of research materials and data’ [(link)](https://static1.squarespace.com/static/5d3799de845604000199cd24/t/637acd803a7dab6b8a27dfd3/1668992386691/fgene-13-823309.pdf) |FTR | 
| Context for use, use documentation   | Any avilable information on who this was built for and why, including tutorials or support resources        | FTR           |
|



## Maximal / Project-Specific Categories 

Information coming soon -- [refer to this document in interim](https://docs.google.com/document/d/1YyP6ppfDu3CIBcKR0Nw_STgNd7EMMtwiJu0kFXLWkxQ/edit). 


### Project-specific Conservation Benefits Catalog Questions

| Category             | Description                          | 
| -------------------          | -----------                          | 
| Organization Category                 | Options include  Academia, Not for Profit, NRCS, Other USDA, State Government, Other Government    |   
| Land Use Category                 |  Cropland, Rangeland, Pasture, Forest, Farmstead, Associated Ag Land, Other                 |
| Dataset Geographic area                 |   48 conterminous states, Hawaii, Alaska, US territories, and/or more specific answers like a subset of states or regions                |    
| Spatial Grain                 | State, County, Watershed, Field, Point, Individual, or specific region (e.g. California)                  |
| Spatial Extent                 |  Global, National, Regional, or specific region (e.g. US West)                 |
| Resource Concern / SWOP categories        | NRCS categories of Soil, Water, Air, Plants, Animals, Energy, Human, Climate | 
| Primary Model Outputs                |  Categories are: air quality, biodiversity, crop productivity, economic analyses, energy efficiency, erosion, fire behavior, GHG emissions, hydrology, land use/cover, livestock productivity, manure management, N cycling and losses, P cycling and losses, pest management, pesticide pollution, socio-economic, soil carbon, soil properties, soil water, water quality. [See .CSV here](https://gitlab.com/OpenTEAMAg/fair-tech-registry/devs/-/raw/392c786602c08df8ff6e875490f5a0756b88b7b5/docs/assets/Primary%20model%20outputs.csv?inline=false).  |
| Dataset Start Date                 |                   |
| Dataset End Date                |                   |
| Strengths and Weaknesses               | Work began to sort using this framework, but was outside the scope of current agreements. Could be a useful place to pick up additional project work in the future. |

### Fair Tech Registry Tier 2 Questions

These are the additional questions derived from FAIR Tech Registry Collabathon discovery that we saw as providing increased FAIRness, but that might not be possible or relevant for most projects to disclose. Any information you can provide about your tool or asset in these categories is valuable. Keeping all of this information up to date is also an intensive process - such that having answers to these questions can also be a functional way for users to assess maturity, useability, and FAIRness of tools listed. 

#### Technical Context
| Category             | Description                          | 
| -------------------          | -----------                          | 
| Ability to localize?                |   |
| Security Update Information               |   |
| Is the source code publicly listed?               | Provide link(s) if so |
| Data Extraction                |  Description of how data can be exported, file formats for export |
| Supported systems/environments               |   |

#### Business, Legal, Contractual Context
| Category             | Description                          | 
| -------------------          | -----------                          | 
| Terms of Use               |   |
| List of compatible licenses               |   |
| Data storage information               |   |
| Privacy protection statement             |   |
| Policies               | Ethics policy / statement, Codes of Conduct or other guiding documents followed  |
| Describe the mechanism for ‘Disclosure, consent, and control'               | With regard to secondary uses - this is crucial for alignment with [CARE Principles](https://www.gida-global.org/care)  |
| Mission Statement              |  Including statement of purpose, goals, and audience, if applicable. |

#### Social and Community Context
| Category             | Description                          | 
| -------------------          | -----------                          | 
| Referencing / Attribution instructions               |   |
| Context for interpretation               |   |
| Community Size              | Include number of users and community support resources, if applicable  |
| Pre-requisites for use, skill level needed            |   |
| Anticipated changes              | Is this resource static, or under continual development?  |
| List some analogous /‘alternatives-to’ the module               |  |
| Details on how to contribute              |   |
| Tool Compatibility              | Compatibility with other listed modules & tools in ecosystem, and/or documentation of how to use the product with others  |
| Practice Compatibility             | Documentation describing shared taxonomy of practices, shared ontologies, or shared methodologies, as applicable.   |


## Criteria Resources

A version of the minimal and maximal criteria for FAIR Tech Registry listing is available as a [SurveyStack survey.](https://app.surveystack.io/surveys/66561506561ed665e4fbdf4c) Entries into this survey will populate the next version of the Registry prototype.  




<iframe width="800" height="600" src="https://app.surveystack.io/surveys/66561506561ed665e4fbdf4c" title="SurveyStack Survey" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Other formats

* [Google Sheets Template](https://docs.google.com/spreadsheets/d/19iOglEfxKtyq0S-klEGYFj8tgs6Px1A8GMDbkfR3EPI/edit?gid=0#gid=0)
* [Excel Template](https://gitlab.com/OpenTEAMAg/fair-tech-registry/devs/-/raw/2ee37118f7dcfa886fb0d6a2d014289369d6f92c/docs/assets/Fair%20Tech%20Parameters%20of%20Assessment%20-%20Worksheet%20Template.xlsx?inline=false)
* [Google Form Survey](https://docs.google.com/forms/d/e/1FAIpQLSfHDUYmxJKYt8nTALkLJMYAmJErLFXEfRoT7HDg1ABnWXKfmQ/viewform)
* [Google Doc - Printable worksheet](https://docs.google.com/document/d/1ldkZstWAjaWbkdcEq-GTzBejDLgb5hp8G2_k7HPJUqA/edit)
* [PDF - Printable worksheet](https://gitlab.com/OpenTEAMAg/fair-tech-registry/devs/-/blob/2ee37118f7dcfa886fb0d6a2d014289369d6f92c/docs/assets/Printable%20-%20FAIR%20Tech%20Registry%20Criteria%20Questions.pdf)
* [PDF - SurveyStack survey version (includes dropdown lists)](https://gitlab.com/OpenTEAMAg/fair-tech-registry/devs/-/blob/2ee37118f7dcfa886fb0d6a2d014289369d6f92c/docs/assets/Listing%20Criteria%20-%20FAIR%20Tech%20Registry%20Catalog%20-%20SurveyStack.pdf)