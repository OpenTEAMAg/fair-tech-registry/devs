---
date:
  created: 2024-05-15
---

# Choosing Tooling Scope 

As we explored the question of what to include in our initial prototype of NRCS tools, we faced several challenges and decision points. 

Initial conversations included the following critical scope questions: 
- If we surveyed modelers within NRCS, which ones would they say to include?  
- What would provide the most utility to have completed, across projects?  
- What tools have we already begun to categorize as a result of internal project needs?
- How does the NRCS label of 'tool' differ from conventional understanding of what a tool is? 


<!-- more -->

The last point was a particular area of tension. The way that the NRCS conceptualizes data tools leads to an abundance of specific data tools, all characterized & described (if at all!) in a way that is oriented toward their initial creation, rather than toward ecosystem-wide utility or FAIR principles. 


Another revealed challenge is that there are a few tools that are very heavily used by many different consumers, and many, many tools that are heavily used by a few specific consumers. We want to both start with a manageable scope, relive common pain points, and provide the ability to scale based on emergent needs. 

While the result that would be most useful for the IRA GHG Database project would be a list of *all* ideal assets, we proposed that we start with tooling that is mentioned across multiple tool lists. The lists we compared were: 

 - The PCSC Data Dictionary
 - The Meridian Conservation Benefits Catalog
 - The IRA-GHG Blueprint tool list
 - The Conservation Costs Database discovery interviews

Once this list is drafted, we will consult relevant experts to see if there are additional tools that should be included in a preliminary assessment. One intended output of this project is a process & support documentation so that others can contribute tool listings or metadata to this registry as needed, so our focus is on producing and solidifying requirements and documentation for quadrant one tooling, and then using our findings to develop functional support documentation so that anyone with relevant interest and expertise could contribute to quadrants two, three, and four. 

 ![Quadrants](https://gitlab.com/OpenTEAMAg/fair-tech-registry/devs/-/raw/5cb352b5d046dcbda1fdc81c3333c5dc32f9d51a/docs/assets/quadrants.png)

*Screenshot illustration from brainstorm meeting to determine registry prototype scope.* 


![Scope](https://gitlab.com/OpenTEAMAg/fair-tech-registry/devs/-/raw/5cb352b5d046dcbda1fdc81c3333c5dc32f9d51a/docs/assets/scope.png)

*Visual by Melanie Kammerer showing relationships between assets in this ecosystem.* 



## Proposed tooling

 Primary priority list: Tools mentioned 3+ times across separate discovery processes, with relatively high levels of extant documentation & documentation need: 

 
- APEX
- CART
- CEAP
- CD
- COMET Farm
- COMET Planner
- CPDES
- DNDC
- Farmers.gov
- RUSLE2

Note: We will also including PODS / farmOS tooling as an integral part of our work, but will use these extant tools to determine the best or most accessible way to list those flexible components. 

We have several secondary priority lists composed of tooling mentioned twice or more across discovery processes, but with challenges for accessing or categorizing documentation / documentation needs. These may also be prioritized based on initial project feedback and focus areas, such as direct GHG measurement vs calculated costs or benefits.  


### May 22nd update - 
Subsequent discussion on May 22nd included the possibility of the FINBIN/FINPAK team as potential collaborators, and integrating their needs & priorities in conjunction with PODS team. This discussion covered inputs, outputs, user-accessible/useable tools, & clarifying relationships between tools as priority for v1 

### June Updates - 

As NRCS project partners considered reconfiguration, we discussed shifting focus from components of tooling relevant to IRA-GHG toward larger NRCS GHG projects that sit below the full-project level.  
