---
date:
  created: 2024-09-04
---

# Community Review Session -  Criteria

On August 20th, 2024, OpenTEAM held a community review session to provide updates on the FAIR Tech Registry, with the following agenda:  

- ​Review progress on FAIR Tech Registry work from February to July
- Discuss partnerships & cooperative agreements supporting this work
- Present documentation of community-ready resources
- Discuss future project stages and applications


<!-- more -->


[Luma Event Link](https://lu.ma/vrfrc55i)

[Miro Board for Event](https://miro.com/app/board/uXjVKy6Nym8=/)

Vic from OpenTEAM introduced the session and provided background and context on the work to date. Melanie Kammerer of EcoData and Meridian Institute shared the intersecting work happening under their cooperative agreement on the Conservation Benefits Database, and Juliet Norton of Purdue presented on the PODS project challenges of documentation for a specific modular component, the MODUS Methods Library.

We then presented the set of criteria developed & synthesized in previous months from across our projects, and prompted group with the following discussion questions: 

### How we can lower the barriers for getting these questions answered?

**Answers**: 

- Prepopulate with publically available material (maybe use AI to skim thru websites, etc)  
- Focus groups / workshops in which  reps from many institutions come together to learn about FAIR and then register their product in the registry, doing a bit of a self assessment, getting kind of a "report" or "roadmap for improvement" as an output  
- Community reviews - aggregate common threads into FAIR criteria  
- Opportunity for community hackathon or other event to rapidly accumulate drafts of registry entries (reviewing public info). 
- Can we find an existing place where the people we want involved are already gathering to leverage or partner with?  
- Need to identify which kinds of information tend to be publicly available and can (more) reliably be pre-populated. Demonstrates that we've done our homework and our work. Also demonstrates how they are evaluated based on publicly available information.  
 
- Develop a reasonable "first guess" from public documentation, then review by technical experts. Lower burden on experts by having an initial draft of answers to review.  
- One thing that of note that came up with Modus - when we tried to prepopulate lab method information, lab personnel found it more difficult to edit incorrect info, there was less confusion entering it all in fresh themselves 


### What strategies can we use to collect, ingest, or find this information-- wherever it exists?

**Answers:** 

- If people are using this to make decisions, that provides incentives for tool creators to put their info on the registry  
- Provide incentives for identified tool creators/maintainers to create listings/deliver the needed detail  
users of tooling providing feedback / counter evidence of FAIR  (i.e., the way the product team presents themself may be a bit better than reality) 😬  
- One potential incentive is data re-use; if I provide and update information in your system, will you re-publish it in an open form that can be used elsewhere? (through JSON format that can be re-accessed)
- Efficiency as a strong incentive - making it easier to fill gaps, make connections, move information  
- Is this a classic chicken/egg problem where if there's a resource that people are using, then others will be motivated to maintain, or does it need more "help" to get started?  
Gamification can be an incentive (top user/contributor badges, etc.) that works to some degree in other community science type projects  
- Networking is an incentive (building community to connect otherwise potentially isolated individuals)

### What's next?
We then discussed what application or entry into a FAIR Tech Registry might look like for user tools, shared examples of overlapping relevant work, and discussed additional areas of focus for future sessions, including the following: 

* Assessment of how many distinct registries, vs how many ways of filtering, are necessary for users
  * Lumping vs splitting of project entries and categories 
  * Guidelines for differentiating unique instances based on user needs  

* Creation of user guides
* Registries & tools outside of the US

 ![Vision](https://gitlab.com/OpenTEAMAg/fair-tech-registry/devs/-/raw/cb55503dbed72ccb6852b62e8df4f62513e96bb3/docs/assets/vision.png)

*Visual by Melanie Kammerer, created for NRCS Project Briefing 8.13*



