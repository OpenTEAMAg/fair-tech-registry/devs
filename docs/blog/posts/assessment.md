---
date:
  created: 2024-06-05
---

# Parameters of Assessment

As with choosing tools, we began with comparing what was already in development for internal projects. Both the FAIR Tech Registry Collabathon and the Conservation Benefits Catalog produced tentative categories for assessing tools or models, based on community fact-finding. We compared the column headers in use in the CBC with the Criteria for Evaluation list from the FTR, and highlighted areas of overlap. 


<!-- more -->


We then focused on the categories that both the Fair Tech Registry and the Conservation Benefits Catalog had put forward as required or most relevant information to include. This version of initial criteria provided the groundwork for the following table: 

![Critieria - Tier 1](https://gitlab.com/OpenTEAMAg/fair-tech-registry/devs/-/raw/12867e27430cdb1a1af51f1dd164cee989058b38/docs/assets/criteria.png)

[Parameters of Assessment Worksheet & Evaluation spreadsheet available here.](https://docs.google.com/spreadsheets/d/12n2J38W3xFIezXSS0wtx5obUhYah8MVi_Lh2vH8EclY/edit?gid=0#gid=0)

This question set is also available as a [SurveyStack survey](https://app.surveystack.io/surveys/66561506561ed665e4fbdf4c). 

We discussed ways of documenting ideal entry metadata, while also not excluding tools that are not comprehensively described. We also considered ways of conveying that our objective is to create a pathway toward increased FAIRness, and simplify and centralize what information is necessary and useful to declare about a tool, rather than introduce a standard for what FAIR is in this context.   
