---
date:
  created: 2024-04-16
---

# Convening Contributors 

From the start of OpenTEAM’s work on the FAIR Tech Registry (FTR) Collabathon, we anticipated that this project would have direct applicability to the needs resulting from CEMA 221. Upon conclusion of the collabathon in January of 2024, we began to apply our findings to an applicable use case. 


<!-- more -->


From Collabathon final report: 

*“Our preliminary test case will involve work done as part of a cooperative agreement between NRCS and OpenTEAM focused on the Producer Operations Data system (PODS), schema creation tooling, and Conservation Cost Database work. We will coordinate with these project teams to define modular components of these tools, using the framework and criteria developed over the Collabathon term, and assess them as a means for negotiating shareability and interoperability across USDA and NRCS.”* 

We saw this ongoing work as an opportunity to both test our assumptions agains projects within our immediate network, and weave those projects together. These projects are:  
 - PODS and Conservation Costs Database cooperative agreements (including OpenTEAM, Our-Sci, Purdue) 
- Meridian’s Conservation Benefits Catalog
- IRA-GHG Data Blueprint work 
	
We began with initial conversations in January and February 2024 to lay the groundwork and context for collaboration. As we built our understanding, we transitioned to recurring meetings in March through May 2024. 

We integrated this project group with PODS weekly meetings from April 2024 onward, and will continue to schedule ad hoc meetings to coordinate across OpenTEAM, Meridian, and NRCS stakeholders as needed. 
