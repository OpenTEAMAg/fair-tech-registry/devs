---
date:
  created: 2024-06-13
---

# Applying critieria to extant tooling

Following discussion with the PODS team, we scheduled a meeting with Greg Austic & Juliet Norton to test the parameters we had developed around tooling & listing criteria for existing relevant modules they have developed.

Significant discussion finding was that this initial prototype registry should be geared toward and thus include a distinct listing question describing what a given module needs for further development or adoption. 


<!-- more -->

  (Modus & conventions) 

 We added the highlighted categories as important for attestation for the specific project components we were documenting as initial examples. 
