# Frequently Asked Questions

## What is OpenTEAM? 

OpenTEAM (Open Technology Ecosystem for Agricultural Management) is a community-led effort facilitated by [Wolfe’s Neck Center for Agriculture and the Environment](https://www.wolfesneck.org/). 

The broad mission of the OpenTEAM community is to create an open source, interoperable tech ecosystem that enables farmer-control of data, knowledge sharing, and access to programs and marketplace incentives. 

 By acting as a convener, technology steward, and facilitator, OpenTEAM equips food system leaders with shared knowledge, collaborative frameworks, and open-source, connected technologies to support farmer viability, thriving ecosystems and vibrant communities.



- [Our website](https://openteam.community/)
- Email us at [info@openteam.community](mailto:sinfo@openteam.community)

## What are FAIR Principles?

The FAIR principles were published in 2016 in a paper called "[The FAIR Guiding Principles for scientific data management and stewardship](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4792175/)" with the goal of 'improv[ing] the infrastructure supporting the reuse of scholarly data.'

The four foundational principles are Findability, Accessibility, Interoperability, and Reusability. These each have a series of sub-principles which you can find [here via Go-FAIR.](https://www.go-fair.org/fair-principles/). These four principles are guidelines, with a wide range of context-specific possibilities for application, and many different (even conflicting) ways to implement them in a given area. 

## What is FAIR in Practice? 

FAIR in Practice is the way that OpenTEAM specifically seeks to apply the FAIR principles to projects through documentation, experimentation, and communication.

In order for us to enact the FAIR principles across our community, we are focused on building methods to make these concepts broadly comprehensible for multiple actors across the open-source, agriculture, and tech realms. This includes: 

* Building examples and test cases based on real user needs 
* Attending to process documentation
* Designing methods for sharing information across contexts and organizations
* Developing and deploying new schemas & ontologies
* Generating community buy-in through collaborative experimentation



## What is the FAIR Tech Registry? 

This is an early-stage project we are building toward in conjuntion with many partners, as part of a series of long-term goals. When and if we achieve the broad vision of creating a FAIR Tech Registry, we envision a searchable, curated list of tools and components that comply with FAIR principles, with built-in support structures & requirements for describing what each entry is for, how it interacts with other tools and data, and how it can be built on top of or repurposed.

This includes the creation of social, legal, technical, and operational frameworks that make each of these statements meaningful and enforceable in practice. There are many other aspects of this work that we must develop in tandem in order to make this vision possible, including codesign around data rights, data justice, consent management, identity services, and more. 

## Ok, so how does this specific project scope fit into those?

At our current stage, we are working to prototype an initial 'alpha' version of  a FAIR Tech Registry specific to NRCS-adjacent data tools & components, with application for greenhouse gas or ecosystem service benefits quantification. This narrow 'slice' of a registry will provide us with information we need to both develop internal standards and documenation in this area, and also function as a use case or example of what we need to build to expand into future registry development. That future development will have its own defined scope & focus, and likely include new and expanded partnerships and agreements. 

## What is the Conservation Benefits Catalog?

 The Conservation Benefits Catalog, or CBC, documents 175+ datasets, models, and tools relevant for assessing outcomes of NRCS conservation practices. It was produced as an outcome of a cooperative agreement between the USDA, NRCS, and [Meridian Institute](https://merid.org/), beginning in 2023. In the first six months of 2024, the project added several new GHG models to the Conservation Benefits Catalog based on recommendations from the NRCS team working on the IRA GHG mitigation project.

Work on the FAIR Tech Registry prototype for NRCS to date has been an effort to bring our collaborations together into a 'superproject' that is greater than the sum of its parts. 

As Meridian describes the potential of this project collaboration, "...this catalog [the CBC] could serve as an initial list of assets to include and a use case of how to organize information for a specific partner (e.g., NRCS) and scope of work (conservation benefits). Conversely, integrating catalog records into the FAIR Tech Registry would solidify information necessary to include and subject catalog content to a critical step of community review, greatly increasing trust in the resulting product." -  *Quantifying Benefits: A Path Forward for an Integrated Approach to NRCS Conservation Assessment, Meridian Institute, February 2024* 




## What do you mean by "reference implementation"? 

Formally, we use 'reference implementation' to mean a piece of software, standard, or process that demonstrates necessary requirements and can be adopted by others. 
These functional prototypes are intended to help others implement their own version of the specification or find problems during the creation of a more mature tool.

We use this language to distinguish the role we're playing. OpenTEAM is not trying to promote any specific tool or project. Instead, we focus on testing and experimenting with new structures for managing agricultural data or coordinating resources. In order to show the utilities or processes we're interested in, we need tools as a place to work and create proofs of concepts-- but our ultimate goal is for these developments to be applied across & beyond the original tool or system they were tested in. 

## What is a Cooperative Agreement?

[Per the USDA's definition](https://sandbox.fsa.usda.gov/programs-and-services/cooperative-agreements/index#coopAgree), "A cooperative agreement reflects a relationship between the U.S. Government and a recipient and is used when the government's purpose is to assist the intermediary in providing goods or services to the authorized recipient." In practical terms, it's a partnership between the government and a non-government entity to make something potentially useful. 

For example, does this ring a bell? 

![Arthur, via PBS](https://gitlab.com/OpenTEAMAg/fair-tech-registry/devs/-/raw/master/docs/assets/via%20pbs.webp?ref_type=heads)
"Funding for Arthur is provided by a Ready to Learn Television Cooperative Agreement from the US Department of Education through the Public Broadcasting Service, and by contributions to your PBS station from Viewers Like You. Thank You!" [*(1)*](https://wkbs-pbs-kids.fandom.com/wiki/Arthur_Funding_Credits)




## How can I get involved? 

If this work is relevant to you, we would love to be in touch! Please contact vic@openteam.community to share your interest. You can also find out more about OpenTEAM at our website, [OpenTEAM.community](openteam.community). 
